package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindow {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/loginHome.jsf");
		driver.manage().window().maximize();
		driver.findElementByXPath("//span[text()='AGENT LOGIN']").click();
		Thread.sleep(3000);
		driver.findElementByLinkText("Contact Us").click();;
		Set<String> windowHandles = driver.getWindowHandles();
		System.out.println(windowHandles.size());
		List<String> listWindows = new ArrayList<String>();
		listWindows.addAll(windowHandles);
		String ContactUs = listWindows.get(1);
		driver.switchTo().window(ContactUs);
		System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		//driver.close();
		String FirstWindow = listWindows.get(0);
		driver.switchTo().window(FirstWindow);
		driver.findElementById("usernameId").sendKeys("Bindhu");
		for (String string : windowHandles) {
			driver.switchTo().window(string);
			System.out.println(driver.getTitle());
		}
		System.out.println(driver.getTitle());
	}

}

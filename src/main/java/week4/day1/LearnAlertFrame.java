package week4.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnAlertFrame {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();
driver.switchTo().alert().sendKeys("Madhumitha");
driver.switchTo().alert().accept();
String text = driver.findElementById("demo").getText();
System.out.println(text);
driver.switchTo().defaultContent();
driver.findElementById("menuButton").click();

	}

}

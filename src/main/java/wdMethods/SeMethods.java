package wdMethods;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.sound.midi.SysexMessage;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import week5.day1.LearnHtmlReportData;

public class SeMethods extends LearnHtmlReportData implements WdMethods{
	public int i = 1;
	public RemoteWebDriver driver;
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")){
				ChromeOptions opt=new ChromeOptions();
				opt.addArguments("--disable-notifications");
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver(opt);
			} else if (browser.equalsIgnoreCase("firefox")){
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("The Browser "+browser+" Launched Successfully");
		} catch (WebDriverException e) {
			System.err.println("The Browser "+browser+" not Launched");
		} finally {
			takeSnap();
		}

	}

	
	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id"	 : return driver.findElementById(locValue);
			case "class" : return driver.findElementByClassName(locValue);
			case "linktext": return driver.findElementByLinkText(locValue);
			case "xpath" : return driver.findElementByXPath(locValue);
			}
		} catch (NoSuchElementException e) {
			System.err.println("The Element is not found");
		} catch (Exception e) {
			System.err.println("Unknown Exception ");
		}
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		
		try {
			return driver.findElementById(locValue);
		} catch (NoSuchElementException e) {
			System.err.println("The Element is not found");
		}finally {
			takeSnap();
		}
		return null;
	}

	
	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			stepTestcase("The data \"+data+\" is Entered Successfully", "pass");
			//System.out.println("The data "+data+" is Entered Successfully");
		} catch (WebDriverException e) {
			stepTestcase("The data is not Entered Successfully", "fail");
			//System.out.println("The data "+data+" is Not Entered");
		} finally {
			takeSnap();
		}
	}

	
	public void clickWithNoSnap(WebElement ele) {
		try {
			ele.click();
			System.out.println("The Element "+ele+" Clicked Successfully");
		} catch (Exception e) {
			System.err.println("The Element "+ele+"is not Clicked");
		}
	}
	
	
	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			System.out.println("The Element "+ele+" Clicked Successfully");
		} catch (WebDriverException e) {
		System.err.println("The Element "+ele+"is not Clicked");
		} finally {
			takeSnap();
		}
	}

	@Override
	public String getText(WebElement ele) {
		String text = null;
		try {
			//String currentwin = driver.getWindowHandle();
			//driver.switchTo().window(currentwin);
			 text= ele.getText();
			System.out.println("The text displayed in given element is "+text);
		} catch (Exception e) {
		System.out.println("There is no text present");
		}finally {
			takeSnap();
		}
		return text;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select dd = new Select(ele);
			dd.selectByVisibleText(value);
			System.out.println("The DropDown Is Selected with VisibleText "+value);
		} catch (Exception e) {
			System.err.println("The DropDown Is not Selected with VisibleText "+value);
		} finally {
			takeSnap();
		}

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select dd1=new Select(ele);
			dd1.selectByIndex(index);
			System.out.println("The DropDown Is Selected with Index "+index);
		} catch (Exception e) {
			System.err.println("The DropDown Is not Selected with Index "+index);
		}finally {
			takeSnap();
		}

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		//String currentwindow = driver.getWindowHandle();
		//driver.switchTo().window(currentwindow);
		String title = driver.getTitle();
		System.out.println("The current window title is "+title);
		if(title.contains(expectedTitle)) {
			
			System.out.println("Title matched");
			return true;
		}else
			System.out.println("Title not matched");
		return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		String text = ele.getText();
		if(text.equals(expectedText))
		{
		System.out.println("The given text is matched");
		}else
			System.out.println("Not matched");

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		String text1 = ele.getText();
if(text1.contains(expectedText)) {
	System.out.println("Partially matched");
}else
	System.out.println("Partially not matched");
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		String data = ele.getAttribute(attribute);
data.equals(attribute);
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {
		if(ele.isSelected())
				{
			System.out.println("The element is selected");
				}else
					System.out.println("Not selected");

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		if(ele.isDisplayed()) {
			System.out.println("The element is displayed");
		}else
			System.out.println("The element is not displayed");

	}

	public void switchToFrame(int index) {
		driver.switchTo().frame(index);
}

	@Override
	public void switchToFrame(WebElement ele) {
		driver.switchTo().frame(ele);

	}

	@Override
	public void acceptAlert() {
		driver.switchTo().alert().accept();

	}

	@Override
	public void dismissAlert() {
		driver.switchTo().alert().dismiss();

	}

	@Override
	public String getAlertText() {
		String text1 = driver.switchTo().alert().getText();
		System.out.println(text1);
		return null;
	}

	
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			System.err.println("IOException");
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		driver.close();
	}

	@Override
	public void closeAllBrowsers() {
driver.quit();
	}


	@Override
	public void switchToWindow(int index) {
		Set<String> windowHandles = driver.getWindowHandles();
		System.out.println(windowHandles.size());
		ArrayList<String> arrayList = new ArrayList<String>();
		arrayList.addAll(windowHandles);
		String currentwin1 = arrayList.get(index);
		driver.switchTo().window(currentwin1);
	}

}

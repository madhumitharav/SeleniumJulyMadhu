package week2day2;

import java.util.Scanner;

public class ArithmeticUser {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Print the first input:");
		int a=scan.nextInt();
		Scanner scan1 = new Scanner(System.in);
		System.out.println("Print the second input:");
		int b=scan.nextInt();
		Scanner scan2 = new Scanner(System.in);
		System.out.println("Print the arithmetic operation to be performed:");
		String operationtoperform=scan.next();
		switch(operationtoperform)	
		{
		case "add":
			System.out.println("The addition operation of given input is "+(a+b));
			break; 
		case "sub":
			System.out.println("The subtraction operation of given input is "+(a-b));
			break;
		case "mul":
			System.out.println("The multiplication operation of given input is "+(a*b));
			break;
		case "div":
			System.out.println("The division operation of given input is "+(a/b));
			break;
		default:
			System.out.println("Invalid input");
		}
		scan.close();
		scan1.close();
		scan2.close();
	}
}
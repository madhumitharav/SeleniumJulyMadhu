package week2day2;

import java.util.Scanner;

public class FizzBuzz {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Print the first input:");
		int a=scan.nextInt();
		Scanner scan1 = new Scanner(System.in);
		System.out.println("Print the second input:");
		int b=scan1.nextInt();
		for (int i=a; i<=b;i++){
			if(i%3==0&i%5==0)
				System.out.println("FIZZBUZZ ");
			else if(i%3==0)
				System.out.println("FIZZ ");
			else if(i%5==0)
				System.out.println("BUZZ ");
			else
				System.out.println(i);
		}
		scan.close();
		scan1.close();

	}

}


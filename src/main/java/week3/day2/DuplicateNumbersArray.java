package week3.day2;

import java.util.LinkedHashSet;
import java.util.Set;

public class DuplicateNumbersArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a = {13,15,67,88,65,13,99,67,65,87,13};
		int n=a.length;
		//to print the duplicated numbers only once, we need to use Array.sort, Set concept or String's inbuilt functions
		//Option # 1
		for(int i=0;i<n;i++) {
			for(int j=i+1;j<n;j++) {
			if(a[j]==a[i]) {
				System.out.print(a[i]+" ");
					break;
				}
			}
		}
		
	}

}

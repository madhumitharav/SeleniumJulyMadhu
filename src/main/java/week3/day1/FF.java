package week3.day1;


import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.GeckoDriverService;
import org.openqa.selenium.support.ui.Select;

public class FF {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.gecko.driver","./drivers/geckodriver.exe");
		FirefoxDriver driver=new FirefoxDriver();
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.manage().window().maximize();
		driver.findElementByPartialLinkText("AGENT").click();
		driver.findElementByPartialLinkText("Sign").click();
		driver.findElementById("userRegistrationForm:userName").sendKeys("MADHU227");
		driver.findElementById("userRegistrationForm:password").sendKeys("Madhu123");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("Madhu123");
		WebElement sec = driver.findElementById("userRegistrationForm:securityQ");
		Select dd = new Select(sec);
		dd.selectByIndex(1);
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("panda");
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Madhumitha");
		driver.findElementById("userRegistrationForm:middleName").sendKeys("-");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("R");
		driver.findElementById("userRegistrationForm:gender:1").click();
		driver.findElementById("userRegistrationForm:maritalStatus:1").click();
		WebElement sec1 = driver.findElementById("userRegistrationForm:dobDay");
		Select dd1 = new Select(sec1);
		dd1.selectByValue("22");
		WebElement sec2 = driver.findElementById("userRegistrationForm:dobMonth");
		Select dd2 = new Select(sec2);
		dd2.selectByValue("07");
		WebElement sec3 = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select dd3 = new Select(sec3);
		dd3.selectByValue("1993");
		WebElement sec4 = driver.findElementById("userRegistrationForm:occupation");
		Select dd4 = new Select(sec4);
		dd4.selectByIndex(4);
		driver.findElementById("userRegistrationForm:uidno").sendKeys("7589412");
		driver.findElementById("userRegistrationForm:idno").sendKeys("84796");
		WebElement sec5 = driver.findElementById("userRegistrationForm:countries");
		Select dd5 = new Select(sec5);
		dd5.selectByValue("94");
		driver.findElementById("userRegistrationForm:email").sendKeys("madhumitha227@gmail.com");
		//driver.findElementById("userRegistrationForm:isdCode").sendKeys("91");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("74589621");
		WebElement sec6 = driver.findElementById("userRegistrationForm:nationalityId");
		Select dd6 = new Select(sec6);
		dd6.selectByValue("94");
		driver.findElementById("userRegistrationForm:address").sendKeys("19a");
		driver.findElementById("userRegistrationForm:street").sendKeys("Kul");
		driver.findElementById("userRegistrationForm:area").sendKeys("Med");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("600100",Keys.TAB);
		Thread.sleep(3000);
		//driver.findElementById("userRegistrationForm:statesName").sendKeys("Tamilnadu");
		WebElement sec7 = driver.findElementById("userRegistrationForm:cityName");
		Select dd7 = new Select(sec7);
		dd7.selectByIndex(1);
		Thread.sleep(3000);
		WebElement sec8 = driver.findElementById("userRegistrationForm:postofficeName");
		Select dd8 = new Select(sec8);
		dd8.selectByValue("Medavakkam  S.O");
		driver.findElementById("userRegistrationForm:landline").sendKeys("74589621");
		driver.findElementById("userRegistrationForm:resAndOff:1").click();
	}

}

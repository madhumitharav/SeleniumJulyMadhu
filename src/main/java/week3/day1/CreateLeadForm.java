package week3.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLeadForm {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByPartialLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("IVTL");
		driver.findElementById("createLeadForm_firstName").sendKeys("Madhumitha");
		driver.findElementById("createLeadForm_lastName").sendKeys("R");
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Madhu");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Ravi");
		driver.findElementById("createLeadForm_parentPartyId").sendKeys("123458971");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Miss");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Team Lead");
		driver.findElementById("createLeadForm_departmentName").sendKeys("Testing");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("3.5L");
		driver.findElementById("createLeadForm_sicCode").sendKeys("123");
		driver.findElementById("createLeadForm_description").sendKeys("Necessary Details");
		driver.findElementById("createLeadForm_importantNote").sendKeys("Emp Details");
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("10");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("Lion");
		WebElement src = driver.findElementById("createLeadForm_dataSourceId");
		Select dd = new Select(src);
		dd.selectByVisibleText("Direct Mail");
		WebElement src1 = driver.findElementById("createLeadForm_marketingCampaignId");
		Select dd1 = new Select(src1);
		dd1.selectByValue("CATRQ_AUTOMOBILE");
		WebElement src2 = driver.findElementById("createLeadForm_industryEnumId");
		Select dd2 = new Select(src2);
		dd2.selectByIndex(3);
		WebElement src3 = driver.findElementById("createLeadForm_ownershipEnumId");
		Select dd3 = new Select(src3);
		dd3.selectByIndex(2);
		WebElement src4 = driver.findElementById("createLeadForm_currencyUomId");
		Select dd4 = new Select(src4);
		dd4.selectByValue("INR");
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("91");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("785496321");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("None");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("None");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("None");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("None");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("madhumitha227@gmail.com");
		driver.findElementById("createLeadForm_generalToName").sendKeys("Madhumitha");
		driver.findElementById("createLeadForm_generalAttnName").sendKeys("Ravi");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("19a");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("-");
		driver.findElementById("createLeadForm_generalCity").sendKeys("Chennai");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("600100");
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("-");
		WebElement src5 = driver.findElementById("createLeadForm_generalCountryGeoId");
		Select dd5 = new Select(src5);
		dd5.selectByValue("IND");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement src6 = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select dd6 = new Select(src6);
		dd6.selectByValue("IN-TN");





	}


}

package week2day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ILearn {

	public static void main(String[] args) {
		List<String> learn = new ArrayList<String>();
		learn.add("i");
		learn.add("l");
		learn.add("e");
		learn.add("a");
		learn.add("r");
		learn.add("n");
		learn.add("t");
		learn.add("a");
		learn.add("l");
		learn.add("o");
		learn.add("t");
		learn.add("t");
		learn.add("o");
		learn.add("d");
		learn.add("a");
		learn.add("y");
		Collections.sort(learn);
		System.out.println("The alphabetical order of given");
		System.out.println(learn);
		int size = learn.size();
		System.out.println(size);
		System.out.println("last "+learn.get(15));
	}

}

package week2day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Televisions1 {

	public static void main(String[] args) {
		Set<String> teletv = new TreeSet<String>();
		teletv.add("Samsung");
		teletv.add("Onida");
		teletv.add("Panasonic");
		teletv.add("Sony");
		teletv.add("MI tv");
		System.out.println("The order of added televisions "+teletv);
		teletv.add("Samsung");
		System.out.println("The order of televisions after duplicate tv is added "+teletv);
		List<String> models = new ArrayList<String>();
		models.addAll(teletv);
		int size=models.size();
		System.out.println(size);
		String s = models.get(4);
		System.out.println(s);
	}
}

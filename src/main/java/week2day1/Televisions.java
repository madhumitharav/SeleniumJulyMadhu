package week2day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Televisions {

	public static void main(String[] args) {
		List<String> tele = new ArrayList<String>();
		tele.add("Samsung");
		tele.add("Onida");
		tele.add("Samsung");
		tele.add("Panasonic");
		tele.add("Sony");
		tele.add("MI tv");
		System.out.println("The order of added televisions"+tele);
		int size = tele.size();
		System.out.println("The count of televisions added "+size);
		tele.remove(5);
		System.out.println("After removal of last tv "+tele);
		Collections.sort(tele);
		System.out.println("The alphabetical order of added televisions"+tele);

	}

}

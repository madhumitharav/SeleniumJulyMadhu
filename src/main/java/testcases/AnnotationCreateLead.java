package testcases;


import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class AnnotationCreateLead extends TC001_Login{
	@BeforeTest
	public void setData(){
		testcasename = "TC002_CreateLead";
		testcasedesc = "Create Lead";
		author = "Madhu";
		category = "Functionally";
	}
	
	@Test(invocationCount=2, invocationTimeOut=30000)
	public void createLead() {

		
		WebElement elecrmlink = locateElement("linktext","CRM/SFA");
		click(elecrmlink);
		WebElement elecreateleadlink = locateElement("linktext","Create Lead");
		click(elecreateleadlink);
		switchToWindow(0);
		verifyTitle("Create Lead");
		WebElement elecompnm = locateElement("id","createLeadForm_companyName");
		type(elecompnm, "IVTL");
		WebElement elefirstcompnm = locateElement("id","createLeadForm_firstName");
		type(elefirstcompnm, "IVTL");
		//getText(elefirstcompnm);
		WebElement elesecondcompnm = locateElement("id","createLeadForm_lastName");
		type(elesecondcompnm, "INFO");
		WebElement sourcedd = locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingText(sourcedd,"Direct Mail");
		WebElement sourcedd1 = locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingText(sourcedd1,"Affiliate Sites");
		WebElement elephno = locateElement("id","createLeadForm_primaryPhoneNumber");
		type(elephno, "7845207776");
		WebElement elemailid = locateElement("id","createLeadForm_primaryEmail");
		type(elemailid, "madhumitha22725@gmail.com");
		WebElement eletext = locateElement("createLeadForm_primaryPhoneCountryCode");
		getText(eletext);
		WebElement buttoncreatelead = locateElement("class","smallSubmit");
		click(buttoncreatelead);
		System.err.println("hhhh");
	}

}

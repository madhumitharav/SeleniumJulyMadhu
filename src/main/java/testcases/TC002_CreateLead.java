package testcases;


import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.SeMethods;
import week6_day2.LearnExcelData;

public class TC002_CreateLead extends TC001_Login{
 @BeforeTest(groups = "common")
	public void setData(){
		testcasename = "TC002_CreateLead";
		testcasedesc = "Create Lead";
		author = "Madhu";
		category = "Functionally";
		Excelfilename = "CreateLead";
	}
	
	@Test(dataProvider = "fetchdata")
	public void createLead(String compnm, String firstcompnm, String secondcompnm, String mailid, String phno) {

		
		/*WebElement elecrmlink = locateElement("linktext","CRM/SFA");
		click(elecrmlink);
		WebElement elecreateleadlink = locateElement("linktext","Create Lead");
		click(elecreateleadlink);
		switchToWindow(0);
		verifyTitle("Create Lead");
		WebElement elecompnm = locateElement("id","createLeadForm_companyName");
		type(elecompnm, "IVTL");
		WebElement elefirstcompnm = locateElement("id","createLeadForm_firstName");
		type(elefirstcompnm, "IVTL");
		//getText(elefirstcompnm);
		WebElement elesecondcompnm = locateElement("id","createLeadForm_lastName");
		type(elesecondcompnm, "INFO");
		WebElement sourcedd = locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingText(sourcedd,"Direct Mail");
		WebElement sourcedd1 = locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingText(sourcedd1,"Affiliate Sites");
		WebElement elephno = locateElement("id","createLeadForm_primaryPhoneNumber");
		type(elephno, "7845207776");
		WebElement elemailid = locateElement("id","createLeadForm_primaryEmail");
		type(elemailid, "madhumitha22725@gmail.com");
		WebElement eletext = locateElement("createLeadForm_primaryPhoneCountryCode");
		getText(eletext);
		WebElement buttoncreatelead = locateElement("class","smallSubmit");
		click(buttoncreatelead);
		System.err.println("hhhh");*/
		
		WebElement elecrmlink = locateElement("linktext","CRM/SFA");
		click(elecrmlink);
		WebElement elecreateleadlink = locateElement("linktext","Create Lead");
		click(elecreateleadlink);
		switchToWindow(0);
		verifyTitle("Create Lead");
		WebElement elecompnm = locateElement("id","createLeadForm_companyName");
		type(elecompnm, compnm);
		WebElement elefirstcompnm = locateElement("id","createLeadForm_firstName");
		type(elefirstcompnm, firstcompnm);
		//getText(elefirstcompnm);
		WebElement elesecondcompnm = locateElement("id","createLeadForm_lastName");
		type(elesecondcompnm, secondcompnm);
		WebElement elephno = locateElement("id","createLeadForm_primaryPhoneNumber");
		type(elephno, phno);
		WebElement elemailid = locateElement("id","createLeadForm_primaryEmail");
		type(elemailid, mailid);
		//WebElement eletext = locateElement("createLeadForm_primaryPhoneCountryCode");
		WebElement buttoncreatelead = locateElement("class","smallSubmit");
		click(buttoncreatelead);
	}
		
		
		
	}



package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class TC003_EditLead extends TC001_Login{
	@BeforeTest(groups = "common")
	public void setData(){
		testcasename = "TC002_CreateLead";
		testcasedesc = "Create Lead";
		author = "Madhu";
		category = "Functionally";
		Excelfilename = "EditLead";
	}
	
	@Test(dataProvider="fetchdata")
	public void edit_lead(String FirstName, String Text) throws InterruptedException
	{
		WebElement elink1=locateElement("linktext", "CRM/SFA");
		click(elink1);
		WebElement elink4=locateElement("linktext", "Leads");
		click(elink4);
	    WebElement elink2=locateElement("linktext", "Find Leads");
		click(elink2);
	    WebElement elink3=locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(elink3, FirstName);
	    WebElement elink5=locateElement("xpath", "//button[text()='Find Leads']");
	    click(elink5);
	    Thread.sleep(30000);
	    WebElement elink6=locateElement("xpath", "//table[@class='x-grid3-row-table']//tr//div/a");
	    click(elink6);
	    boolean elink7=locateElement("xpath","//div[text()='View Lead']").isDisplayed();
	    if(elink7==true)
	    {
	    	System.out.println("The title is matched");
	    }
	    else
	    {
	    	System.out.println("The title is not matched");
	
	    }
	    WebElement elink8=locateElement("linktext", "Edit");
	    click(elink8);
	    WebElement elink9=locateElement("id", "updateLeadForm_companyName");
	    elink9.clear();
	    type(elink9, Text);
	    WebElement elink10=locateElement("class", "smallSubmit");
	    click(elink10);
       boolean elink11=locateElement("xpath","//span[contains(text() ,'Syntel')]").isDisplayed();
	    if(elink11==true)
	    {
	    	System.out.println("The company name is matched");
	    }
	    else
	    {
	    	System.out.println("The company name is not matched");
	
	    }
		
	}

}
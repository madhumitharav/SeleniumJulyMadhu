package testcases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import wdMethods.SeMethods;
import week6_day2.LearnExcelData;

public class TC001_Login extends SeMethods{
	@DataProvider(name="fetchdata")
	public Object[][] getdata() throws IOException {
		Object[][] excelData=LearnExcelData.getExcelData(Excelfilename);
			return excelData;
	}
	
	
	@BeforeMethod(groups = "common")
	@Parameters({"browser","URL","UNM","PWD"})
	public void login(String browserNM,String URLNM,String UNM, String PWD ) {
		
		/*startApp("chrome", "http://leaftaps.com/opentaps");
		startTestCase();
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);*/
		startApp(browserNM,URLNM);
		startTestCase();
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, UNM);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, PWD);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
	}
	@AfterTest(groups="common")
	public void closeAllBrowser() {
		closeAllBrowsers();
		System.out.println("Browser closed successfully");
	}

}









package testcases;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
@Test(enabled=false)
public class TC005_MergeLeads {

	public static void main(String[] args) throws InterruptedException, IOException {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByPartialLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("(//img[@alt='Lookup'])").click();
		String parentwindow = driver.getWindowHandle();
Set<String> allwindowHandles = driver.getWindowHandles();
ArrayList<String> win = new ArrayList<String>();
win.addAll(allwindowHandles);
String secwin = win.get(1);
driver.switchTo().window(secwin);
driver.findElementByXPath("//input[@name='id']").sendKeys("10");
driver.findElementByXPath("//button[text()='Find Leads']").click();
Thread.sleep(3000);
driver.findElementByXPath("(//a[@class='linktext'])").click();
driver.switchTo().window(parentwindow);
Thread.sleep(2000);
allwindowHandles.removeAll(allwindowHandles);
win.removeAll(win);
driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
Set<String> allwindowHandles1 = driver.getWindowHandles();
ArrayList<String> win1 = new ArrayList<String>();
win1.addAll(allwindowHandles1);
String secwin1 = win1.get(1);
driver.switchTo().window(secwin1);
driver.findElementByXPath("//input[@name='id']").sendKeys("10");
driver.findElementByXPath("//button[text()='Find Leads']").click();
Thread.sleep(3000);
driver.findElementByXPath("(//a[@class='linktext'])").click();
driver.switchTo().window(parentwindow);
driver.findElementByXPath("//a[text()='Merge']").click();
Thread.sleep(2000);
//driver.switchTo().alert().dismiss();
driver.switchTo().alert().accept();
//allwindowHandles = driver.getWindowHandles();
//win.addAll(allwindowHandles);
//driver.findElementByLinkText("Find Leads").click();
//driver.findElementByXPath("//input[@name='id']").sendKeys("10005");
//driver.findElementByXPath("//button[text()='Find Leads']").click();
Thread.sleep(2000);
//driver.getScreenshotAs(OutputType.FILE);
File src1 = driver.getScreenshotAs(OutputType.FILE);
File des1 = new File("D:\\/snaps/img.png");
FileUtils.copyFile(src1, des1);
Thread.sleep(2000);
driver.close();
//WebDriverWait wait = new WebDriverWait(driver,10);





//driver.findElementByLinkText("10097").click();

	}

}

package homework;

import java.util.Scanner;

public class ReverseOrder {

	public static void main(String[] args) {
		int a=0;
		int temp;
		int b;
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter n");
		int n=scan.nextInt();
		temp=n;
		while(n>0)
		{
			b=n%10;
			a=(a*10)+b;
			n=n/10;
		}
		System.out.println("The reverse order of given input "+temp+ " is "+a);
		scan.close();
	}

}

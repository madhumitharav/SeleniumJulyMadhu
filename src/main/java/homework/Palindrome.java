package homework;

import java.util.Scanner;

public class Palindrome {

	public static void main(String[] args) {
		int a;
		int b=0;
		int rev;
		Scanner scan=new  Scanner(System.in);
		System.out.println("Enter input");
		int input=scan.nextInt();
		rev=input;
		while(input>0)
		{
			a=input%10;
			b=(b*10)+a;
			input=input/10;
		}
		if(rev==b) {
			System.out.println("The given input is a palindrome");
		}
		else
		{
			System.out.println("The given input is not a palindrome");	
		}
		scan.close();
	}

}

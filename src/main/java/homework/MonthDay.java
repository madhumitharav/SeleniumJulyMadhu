package homework;

import java.util.Scanner;

public class MonthDay {

	public static void main(String[] args) {
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter n");
		int n=scan.nextInt();
		switch(n)
		{
		case 1:
			System.out.println("The 1st month of the year is Jan");
			System.out.println("Number of days in the month of Jan is 31");
			break;
		case 2:
			System.out.println("The "+n+"nd month of the year is Feb");
			System.out.println("If the year is not a leap year, Number of days in the month of Feb is 28");
			System.out.println("If the year is leap year, Number of days in the month of Feb is 29");
			break;
		case 3:
			System.out.println("The "+n+"rd month of the year is March");
			System.out.println("Number of days in the month of March is 31");
			break;
		case 4:
			System.out.println("The "+n+"th month of the year is April");
			System.out.println("Number of days in the month of April is 30");
			break;
		case 5:
			System.out.println("The "+n+"th month of the year is May");
			System.out.println("Number of days in the month of May is 31");
			break;
		case 6:
			System.out.println("The "+n+"th month of the year is June");
			System.out.println("Number of days in the month of June is 30");
			break;
		case 7:
			System.out.println("The "+n+"th month of the year is July");
			System.out.println("Number of days in the month of July is 31");
			break;
		case 8:
			System.out.println("The "+n+"th month of the year is Aug");
			System.out.println("Number of days in the month of March is 31");
			break;
		case 9:
			System.out.println("The "+n+"th month of the year is Sep");
			System.out.println("Number of days in the month of Sep is 30");
			break;
		case 10:
			System.out.println("The "+n+"th month of the year is Oct");
			System.out.println("Number of days in the month of Oct is 31");
			break;
		case 11:
			System.out.println("The "+n+"th month of the year is Nov");
			System.out.println("Number of days in the month of Nov is 30");
			break;
		case 12:
			System.out.println("The "+n+"th month of the year is Dec");
			System.out.println("Number of days in the month of Dec is 31");
			break;
		default:
			if(n>12);
			System.out.println("Invalid input");
		}
		scan.close();
	}
}

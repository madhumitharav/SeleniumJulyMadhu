package homework;

import java.util.Scanner;

public class MultiplicationTable {


	public static void main(String[] args) {

		Scanner scan=new Scanner(System.in);
		System.out.println("Enter n");
		int n=scan.nextInt();
		Scanner scan1=new Scanner(System.in);
		System.out.println("Enter m");
		int m=scan1.nextInt();
		int a=1;
		int b=0;
		while(b<m)
		{
			int c=(a*n);
			System.out.println("The multiplication of " +n+ "th table is " + c);
			a=a+1;
			b=b+1;
		}
		scan.close();
		scan1.close();
	}

}

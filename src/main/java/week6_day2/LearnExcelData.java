package week6_day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class LearnExcelData {

	public static Object[][] getExcelData(String fileName) throws IOException {
	   XSSFWorkbook workbook = new XSSFWorkbook("./data/"+fileName+".xlsx");
	   XSSFSheet sheet = workbook.getSheetAt(0);
	   int lastRowNum = sheet.getLastRowNum();
	   System.out.println(lastRowNum);
	   int lastCellNum = sheet.getRow(0).getLastCellNum();
	   Object[][] data= new Object[lastRowNum][lastCellNum];
	   for(int i=1;i<=lastRowNum;i++) {
		   XSSFRow row = sheet.getRow(i);
	   for (int j=0;j<lastCellNum;j++) {
		XSSFCell cell = row.getCell(j);
		String stringCellValue = cell.getStringCellValue();
		data[i-1][j]=stringCellValue;
		System.out.println(stringCellValue);
	   }

	}
workbook.close();
return data;
}
}
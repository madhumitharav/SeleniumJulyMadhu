package week5.day1;

import java.io.IOException;

//import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnHtmlReportTest {
 public static void main(String[] args) throws IOException {
	  
	ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
    html.setAppendExisting(true);
    ExtentReports extent = new ExtentReports();
    extent.attachReporter(html);
    ExtentTest test = extent.createTest("TC001_CreateLead", "Created Lead");
    test.assignAuthor("Madhu");
    test.assignCategory("Function");
    test.pass("Login",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img11.png").build());
    extent.flush();
    System.out.println("completed");
  }
}

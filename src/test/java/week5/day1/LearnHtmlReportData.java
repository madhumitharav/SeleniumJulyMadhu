package week5.day1;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnHtmlReportData {
	public static ExtentReports extent;
	public static String testcasename,testcasedesc,author,category,Excelfilename;
	public static ExtentTest test;
@BeforeSuite(groups = "common")
public void startResult() {
	ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
    html.setAppendExisting(true);
    extent = new ExtentReports();
    extent.attachReporter(html);
}
//@BeforeMethod
public void startTestCase() {
	test = extent.createTest(testcasename,testcasedesc);
	test.assignAuthor(author);
    test.assignCategory(category);
}
public void stepTestcase(String desc, String status) {
	if(status.equalsIgnoreCase("pass"))
		test.pass(desc);
	else
		test.fail(desc);
}
@AfterSuite
public void end() {
	extent.flush();
}

}
